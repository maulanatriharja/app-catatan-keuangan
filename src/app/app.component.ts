import { Component } from '@angular/core';
// import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(
    // public admobFree: AdMobFree,
    public platform: Platform,
  ) {
    // window.addEventListener('keyboardDidShow', () => {
    //   this.admobFree.banner.hide();
    // });

    // window.addEventListener('keyboardDidHide', () => {
    //   this.admobFree.banner.show();
    // });
  }

  ngOnInit() {
    // this.platform.ready().then(() => {
    this.load_admob();
    // this.admobFree.banner.show();
    // });
  }

  ionViewDidEnter() {
    this.load_admob();
    // this.admobFree.banner.show();
  }

  // showAdmob() {
  //   let options = {
  //     adId: 'ca-app-pub-3302992149561172/1858750965',
  //     isTesting: false
  //   }

  //   this.admob.createBanner(options).then(() => { this.admob.showBanner; });
  // }

  load_admob() {
    // const bannerConfig: AdMobFreeBannerConfig = {
    //   isTesting: true,
    //   id: 'ca-app-pub-3302992149561172/1858750965',
    //   autoShow: true,
    //   // forFamily: true,
    //   size: 'BANNER'
    // };
    // this.admobFree.banner.config(bannerConfig);

    // this.admobFree.banner.prepare().then(() => { }).catch(e => console.log(e));
  }

}

