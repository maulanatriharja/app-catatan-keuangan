import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { AlertController, ModalController } from '@ionic/angular';
import { FirebaseService } from '../services/firebase.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';

import * as moment from 'moment';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.page.html',
  styleUrls: ['./form-input.page.scss'],
})
export class FormInputPage implements OnInit {
  @Input() key: any;
  @Input() bulan: any;

  myForm: FormGroup;
  isSubmitted = false;

  constructor(
    public alertController: AlertController,
    public auth: AngularFireAuth,
    public firebaseService: FirebaseService,
    public formBuilder: FormBuilder,
    public modalController: ModalController,
  ) {
    this.myForm = this.formBuilder.group({
      tipe: [''],
      // tanggal: [moment().format('YYYY-MM-DD')],
      tanggal: [moment().format('YYYY-MM-DD')],
      keterangan: [''],
      nominal: [''],
    });
  }

  ngOnInit() {

    if (this.key) {

      // this.myForm = this.formBuilder.group({
      //   tipe: [''],
      //   // tanggal: [moment().format('YYYY-MM-DD')],
      //   tanggal: [this.key],
      //   keterangan: [''],
      //   nominal: [''],
      // });

      this.auth.signInAnonymously().then((res: any) => {
        this.firebaseService.get(res.user.uid, this.key).valueChanges().subscribe(res => {
          this.myForm.setValue(res);
        });
      });
    } else {
      this.myForm = this.formBuilder.group({
        tipe: [''],
        // tanggal: [moment().format('YYYY-MM-DD')],
        tanggal: [this.bulan],
        keterangan: [''],
        nominal: [''],
      });
    }

  }

  ionViewDidEnter() {

  }

  submit() {
    this.isSubmitted = true;

    if (this.myForm.valid) {
      if (this.key) {
        this.firebaseService.update(this.key, this.myForm.value)
          .then((res) => {
            console.log(res)
            this.modalController.dismiss();
          })
          .catch(error => console.log(error));
      } else {

        console.info("this.myForm.value : " + JSON.stringify(this.myForm.value))
        this.firebaseService.create(this.myForm.value).then(res => {
          console.log(res)
          this.modalController.dismiss();
        }).catch(error => console.log(error));
      }
    }
  }

  async hapus() {
    const alert = await this.alertController.create({
      header: 'Konfirmasi',
      message: 'yakin untuk menghapus data ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
        }, {
          text: 'Hapus',
          handler: () => {
            this.auth.signInAnonymously().then((res: any) => {
              this.firebaseService.delete(res.user.uid, this.key);
              this.modalController.dismiss();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  tutup() {
    this.modalController.dismiss();
  }

}
