import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormInputPage } from '../form-input/form-input.page';
import { FirebaseService } from '../services/firebase.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

// import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';

import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

import * as moment from 'moment';

export class DataVal {
  $key!: string;
  tanggal!: string;
  tipe!: string;
  keterangan!: string;
  nominal!: string;
}


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  filter_bulan: any = moment().format('YYYY-MM-DD');

  data: any = [];
  data_filtered: any = [];

  data_tgl: any = [];
  data_tgl_filtered: any = [];

  total_pemasukan: number = 0;
  total_pengeluaran: number = 0;
  saldo: number = 0;

  constructor(
    // public admobFree: AdMobFree,
    public auth: AngularFireAuth,
    public firebaseService: FirebaseService,
    public modalController: ModalController,
    public socialSharing: SocialSharing,
  ) { }

  ngOnInit() {
    moment.locale('id-ID');

    this.auth.signInAnonymously().then((res: any) => {

      function urutTanggal(a: any, b: any) {
        if (a.tanggal < b.tanggal) { return -1; }
        if (a.tanggal > b.tanggal) { return 1; }
        return 0;
      }

      this.fetchData();
      let bookingRes = this.firebaseService.getList(res.user.uid);
      bookingRes.snapshotChanges().subscribe(res => {

        this.data_tgl = [];
        this.data = [];

        this.total_pemasukan = 0;
        this.total_pengeluaran = 0;
        this.saldo = 0;

        res.forEach(item => {
          let a: any = item.payload.toJSON();
          a['$key'] = item.key;
          this.data.push(a as DataVal);
        });
        this.data.sort(urutTanggal)

        this.data_filtered = this.data;

        if (this.data.length > 0) {
          this.data_tgl.push({ tanggal: this.data[0].tanggal });

          for (let i = 0; i < this.data.length; i++) {
            if (i > 0 && this.data[i].tanggal != this.data[i - 1].tanggal) {
              this.data_tgl.push({ tanggal: this.data[i].tanggal });
            }
          }
        }

        this.set_bulan({ value: this.filter_bulan });

        console.info('TANGGAL :', this.data_tgl);
      });
    });
  }

  ionVIewDidEnter() {
    // const bannerConfig: AdMobFreeBannerConfig = {
    //   isTesting: true,
    //   id: 'ca-app-pub-3302992149561172/1858750965',
    //   autoShow: true,
    //   // forFamily: true,
    //   size: 'BANNER'
    // };
    // this.admobFree.banner.config(bannerConfig);

    // this.admobFree.banner.prepare().then(() => { }).catch(e => console.log(e));
  }

  fetchData() {
    this.auth.signInAnonymously().then((res: any) => {
      this.firebaseService.getList(res.user.uid).valueChanges().subscribe(res => {
        console.info('FETCH DATA :', res)
      });
    });
  }

  set_bulan(val: any) {

    this.filter_bulan = val.value.substr(0, 10);

    this.data_tgl_filtered = this.data_tgl.filter((item: any) => {
      return (item.tanggal.substr(5, 2) == val.value.substr(5, 2) && item.tanggal.substr(0, 4) == val.value.substr(0, 4));
    });

    this.data_filtered = this.data.filter((item: any) => {
      return (item.tanggal.substr(5, 2) == val.value.substr(5, 2) && item.tanggal.substr(0, 4) == val.value.substr(0, 4))
    })

    //---TOTAL---

    this.total_pemasukan = 0;
    this.total_pengeluaran = 0;

    for (let i = 0; i < this.data_filtered.length; i++) {

      if (this.data_filtered[i].tipe == 'Pemasukan') {
        this.total_pemasukan += parseInt(this.data_filtered[i].nominal);
      }

      if (this.data_filtered[i].tipe == 'Pengeluaran') {
        this.total_pengeluaran += parseInt(this.data_filtered[i].nominal);
      }
    }

    this.saldo = this.total_pemasukan - this.total_pengeluaran;

  }

  async modalForm(val: any) {

    const modal = await this.modalController.create({
      component: FormInputPage,
      componentProps: {
        key: val,
        bulan: this.filter_bulan
      }
    });
    return await modal.present();
  }

  sharePDF() {

    let items: any = [];

    items.push([
      { text: 'Keterangan', fontSize: 10, style: 'tabel_judul' },
      { text: 'Pemasukan', fontSize: 10, alignment: 'right', style: 'tabel_judul' },
      { text: 'Pengeluaran', fontSize: 10, alignment: 'right', style: 'tabel_judul' },
    ]);

    for (let i = 0; i < this.data_filtered.length; i++) {

      if (this.data_filtered[i].tipe == 'Pemasukan') {
        items.push([
          { text: this.data_filtered[i].keterangan, fontSize: 10, style: 'checked_b', fillColor: '#fff' },
          { text: (this.data_filtered[i].nominal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), fontSize: 10, alignment: 'right', style: 'checked_b' },
          {}
        ]);
      } else {
        items.push([
          { text: this.data_filtered[i].keterangan, fontSize: 10, style: 'checked_b', fillColor: '#fff' },
          {},
          { text: (this.data_filtered[i].nominal).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), fontSize: 10, alignment: 'right', style: 'checked_b' },
        ]);
      }

      // if (i == this.data.length - 1) {
      //   items.push([
      //     { text: 'TOTAL', style: 'row_total' },
      //     { text: '', style: 'row_total' },
      //     { text: '', style: 'row_total' },
      //     { text: ':', style: 'row_total' },
      //     { text: this.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."), style: 'row_total' }
      //   ])
      // }
    }

    let docDefinition: any = {
      pageSize: {
        width: 200,
        height: 'auto'
      },
      pageMargins: [4, 4, 4, 4],
      // pageOrientation: 'landscape',
      content: [
        {
          layout: 'noBorders',
          table: {
            widths: ['*'],
            body: [
              [{ text: 'Catatan Keuangan', fontSize: 10, bold: true, color: '#fff', fillColor: "#34495e", margin: [4, 0, 4, 0] }],
              [{ text: moment(this.filter_bulan).format('MMMM YYYY'), fontSize: 8, bold: true, color: '#eee', fillColor: "#34495e", margin: [4, 0, 4, 0] }],
            ]
          }
        },
        {
          text: " ",
          style: [{ fontSize: 6 }],
        },
        {
          layout: {
            vLineWidth: function (i: any) {
              return 0;
            },
            hLineColor: function (i: any) {
              return (i % 2 === 0) ? '#aaa' : '#fff';
            },
            paddingLeft: function (i: any) {
              return 0;
            },
            paddingRight: function (i: any) {
              return 0;
            }
          },
          table: {
            headerRows: 1,
            widths: ['*', 'auto', 'auto'],
            body: items
          }
        }
      ],
      defaultStyle: {
        fontSize: 10,
        lineHeight: 1.2,
      },
      styles: {
        global_t: {
          fontSize: 10,
          margin: [4, 2, 4, 0]
        },
        global_b: {
          fontSize: 10,
          margin: [4, 0, 4, 2]
        },
        checked_t: {
          color: '#666',
          fillColor: '#eee',
          margin: [4, 2, 4, 0]
        },
        checked_b: {
          color: '#666',
          fillColor: '#eee',
          margin: [4, 0, 4, 2]
        },
        tabel_judul: {
          bold: true,
          color: '#fff',
          fillColor: "#34495e",
          fontSize: 8,
          margin: [4, 4, 4, 4]
        },
        row_total: {
          bold: true,
          color: '#fff',
          fillColor: "#34495e",
          fontSize: 10,
          margin: [4, 6, 4, 2]
        },
      }
    }

    pdfMake.createPdf(docDefinition).getBase64((data: any) => {
      // this.pdfSrc = 'data:application/pdf;base64,' + data;

      this.socialSharing.share(null as any, 'Catatan Keuangan ' + this.filter_bulan, 'data:application/pdf;base64,' + data, null as any).then(() => {
      }).catch(() => { });

      // let blob = this.b64toBlob(base64, 'application/pdf');
    });
  }

}
