import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/compat/database';

export class Data {
  $key!: string;
  tanggal!: string;
  tipe!: string;
  keterangan!: string;
  nominal!: string;
}

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  dataListRef!: AngularFireList<any>;
  dataRef!: AngularFireObject<any>;

  constructor(
    private db: AngularFireDatabase,
  ) { }

  // Get List
  getList(uid: string) {
    this.dataListRef = this.db.list('/' + uid);
    return this.dataListRef;
  }

  // Get Single
  get(uid: string, id: string) {
    this.dataRef = this.db.object('/' + uid + '/' + id);
    return this.dataRef;
  }

  // Create
  create(data: Data) {
    return this.dataListRef.push({
      tanggal: data.tanggal,
      tipe: data.tipe,
      keterangan: data.keterangan,
      nominal: data.nominal,
    })
  }

  // Update
  update(id: any, data: Data) {
    return this.dataRef.update({
      tanggal: data.tanggal,
      tipe: data.tipe,
      keterangan: data.keterangan,
      nominal: data.nominal,
    })
  }

  // Delete
  delete(uid: string, id: string) {
    this.dataRef = this.db.object('/' + uid + '/' + id);
    this.dataRef.remove();
  }
}
